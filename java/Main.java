import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
  public static void main(String[] args) {
    Runnable printCurrentDateTime = () -> System.out.println(LocalDateTime.now());
    Executors
      .newSingleThreadScheduledExecutor()
      .scheduleAtFixedRate(printCurrentDateTime, 0, 1, TimeUnit.SECONDS);
  }
}
