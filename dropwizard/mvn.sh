#!/bin/sh
docker run -it --rm \
  -v "$PWD/.m2":/root/.m2 \
  -v "$PWD":/root/faketime-demo \
  -w /root/faketime-demo maven:3-openjdk-17-slim \
  mvn $@
