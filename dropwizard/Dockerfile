##
## Build
##
FROM maven:3-openjdk-17-slim AS mvn
COPY . /usr/src/build
WORKDIR /usr/src/build
RUN mvn package

FROM openjdk:17-alpine AS faketime
RUN apk update
RUN apk upgrade
RUN apk add bash git build-base
RUN git clone -b 'v0.9.10' --single-branch --depth 1 https://github.com/wolfcw/libfaketime.git
RUN cd /libfaketime/src && make install

##
## Deploy
##
FROM openjdk:17-alpine
COPY --from=faketime /usr/local/lib/faketime /usr/local/lib/faketime
ENV LD_PRELOAD=/usr/local/lib/faketime/libfaketime.so.1
ENV FAKETIME_DONT_RESET=1
ENV FAKETIME_CACHE_DURATION=1
ENV FAKETIME_DONT_FAKE_MONOTONIC=1
ENV FAKETIME_TIMESTAMP_FILE=/var/lib/faketime/faketimerc
COPY --from=mvn /usr/src/build/target/faketime-demo-1.0-SNAPSHOT.jar /faketime-demo.jar
EXPOSE 8080
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/faketime-demo.jar"]
CMD [ "server" ]
