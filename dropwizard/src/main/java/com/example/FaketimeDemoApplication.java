package com.example;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class FaketimeDemoApplication extends Application<FaketimeDemoConfiguration> {

    public static void main(final String[] args) throws Exception {
        new FaketimeDemoApplication().run(args);
    }

    @Override
    public String getName() {
        return "FaketimeDemo";
    }

    @Override
    public void initialize(final Bootstrap<FaketimeDemoConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final FaketimeDemoConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
