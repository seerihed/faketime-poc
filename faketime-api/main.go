package main

import (
	"embed"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/filesystem"
	"github.com/joho/godotenv"
)

//go:embed public/*
var public embed.FS

const pattern = `^(@?\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})|([+-]\d+([.,]\d+)?[mhdy]{1}(\s[xi]\d+([.,]\d+)?)?)$`

func main() {
	_ = godotenv.Load(".env")
	if _, err := os.Stat(os.Getenv("FAKETIME_TIMESTAMP_FILE")); os.IsNotExist(err) {
		os.WriteFile(os.Getenv("FAKETIME_TIMESTAMP_FILE"), []byte("+0d\n"), 0644)
	}
	app := fiber.New()
	app.Use("/", filesystem.New(filesystem.Config{
		Root:       http.FS(public),
		PathPrefix: "/public",
	}))
	app.Get("/api/timestamp", func(c *fiber.Ctx) error {
		c.SendStatus(200)
		c.Set("Content-Type", "text/plain")
		return c.SendFile(os.Getenv("FAKETIME_TIMESTAMP_FILE"))
	})
	app.Post("/api/timestamp", func(c *fiber.Ctx) error {
		if matched, _ := regexp.MatchString(pattern, c.FormValue("value")); matched {
			value := []byte(c.FormValue("value"))
			return os.WriteFile(os.Getenv("FAKETIME_TIMESTAMP_FILE"), value, 0644)
		}
		return fiber.NewError(fiber.StatusBadRequest, "Malformed time string")
	})
	log.Fatal(app.Listen(os.Getenv("HTTP_ADDR")))
}
